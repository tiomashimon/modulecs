﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModuleCs
{
    internal class functions
    {
        public static void FillDataGridView(DataGridView dataGridView, NumericUpDown num)
        {
            Random random = new Random();

            dataGridView.Columns.Clear(); // Очищаємо таблицю
            dataGridView.Rows.Clear();

            int columnCount = Convert.ToInt32(num.Value);
            int rowCount = 1;


            if (columnCount != null)
            {
                // Додавання потрібної кількості стовпців
                for (int j = 0; j < columnCount; j++)
                {
                    // Створюємо новий стовпець
                    DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                    dataGridView.Columns.Add(column);
                }

                // Додавання рядків до DataGridView
                for (int i = 0; i < rowCount; i++)
                {
                    // Створення нового рядка
                    DataGridViewRow row = new DataGridViewRow();

                    for (int j = 0; j < columnCount; j++)
                    {
                        // Створення нової ячейки
                        DataGridViewCell cell = new DataGridViewTextBoxCell();

                        // Генерація рандомного значення
                        int value = random.Next(20);
                        cell.Value = value;

                        // Додавання ячейки до рядка
                        row.Cells.Add(cell);
                    }

                    // Додавання рядка до DataGridView
                    dataGridView.Rows.Add(row);
                }
            }
            else
            {
                MessageBox.Show("Заповни таблицю");
            }


        }
        public static double CalculateAverage(DataGridView dataGridView)
        {
            double sum = 0;
            int count = 0;

            // Проходимося по всім рядкам DataGridView
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                // Проходимося по всім коміркам рядка
                foreach (DataGridViewCell cell in row.Cells)
                {
                    // Перетворюємо значення комірки на double
                    if (cell.Value != null && double.TryParse(cell.Value.ToString(), out double value))
                    {
                        sum += value;
                        count++;
                    }
                }
            }

            // Обчислюємо середнє арифметичне
            if (count > 0)
            {
                double average = sum / count;

                return average;
            }
            else
            {

                return 0;
            }
        }

        public static void NumToZero(DataGridView dataGridView, double num)
        {
            // Проходимося по всім рядкам DataGridView
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                // Проходимося по всім коміркам рядка
                foreach (DataGridViewCell cell in row.Cells)
                {
                    // Перевіряємо, чи комірка містить числове значення
                    if (cell.Value != null && double.TryParse(cell.Value.ToString(), out double value))
                    {
                        // Перевіряємо, чи значення більше заданого порогу
                        if (value > num)
                        {
                            cell.Value = 0; // Замінюємо значення на 0
                        }
                    }
                }
            }
        }



    }

}



﻿namespace ModuleCs
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.vektor = new System.Windows.Forms.DataGridView();
            this.columns = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.Fill = new System.Windows.Forms.Button();
            this.Average = new System.Windows.Forms.Button();
            this.averagenum = new System.Windows.Forms.Label();
            this.change = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.vektor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columns)).BeginInit();
            this.SuspendLayout();
            // 
            // vektor
            // 
            this.vektor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vektor.Location = new System.Drawing.Point(25, 61);
            this.vektor.Name = "vektor";
            this.vektor.Size = new System.Drawing.Size(738, 58);
            this.vektor.TabIndex = 0;
            // 
            // columns
            // 
            this.columns.Location = new System.Drawing.Point(25, 35);
            this.columns.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.columns.Name = "columns";
            this.columns.Size = new System.Drawing.Size(120, 20);
            this.columns.TabIndex = 1;
            this.columns.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Кількість значень у векторі";
            // 
            // Fill
            // 
            this.Fill.Location = new System.Drawing.Point(70, 140);
            this.Fill.Name = "Fill";
            this.Fill.Size = new System.Drawing.Size(75, 23);
            this.Fill.TabIndex = 3;
            this.Fill.Text = "Заповнити ";
            this.Fill.UseVisualStyleBackColor = true;
            this.Fill.Click += new System.EventHandler(this.Fill_Click);
            // 
            // Average
            // 
            this.Average.Location = new System.Drawing.Point(163, 140);
            this.Average.Name = "Average";
            this.Average.Size = new System.Drawing.Size(75, 23);
            this.Average.TabIndex = 4;
            this.Average.Text = "Знайти середнє";
            this.Average.UseVisualStyleBackColor = true;
            this.Average.Click += new System.EventHandler(this.Average_Click);
            // 
            // averagenum
            // 
            this.averagenum.AutoSize = true;
            this.averagenum.Location = new System.Drawing.Point(300, 145);
            this.averagenum.Name = "averagenum";
            this.averagenum.Size = new System.Drawing.Size(124, 13);
            this.averagenum.TabIndex = 5;
            this.averagenum.Text = "Середнє арефметичне:";
            // 
            // change
            // 
            this.change.Location = new System.Drawing.Point(569, 140);
            this.change.Name = "change";
            this.change.Size = new System.Drawing.Size(75, 23);
            this.change.TabIndex = 6;
            this.change.Text = "Замінити";
            this.change.UseVisualStyleBackColor = true;
            this.change.Click += new System.EventHandler(this.change_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.change);
            this.Controls.Add(this.averagenum);
            this.Controls.Add(this.Average);
            this.Controls.Add(this.Fill);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.columns);
            this.Controls.Add(this.vektor);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.vektor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columns)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView vektor;
        private System.Windows.Forms.NumericUpDown columns;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Fill;
        private System.Windows.Forms.Button Average;
        private System.Windows.Forms.Label averagenum;
        private System.Windows.Forms.Button change;
    }
}


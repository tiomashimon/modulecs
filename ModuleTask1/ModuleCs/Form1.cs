﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModuleCs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Fill_Click(object sender, EventArgs e)
        {
            functions.FillDataGridView(vektor, columns);
        }

        private void Average_Click(object sender, EventArgs e)
        {
            double av = functions.CalculateAverage(vektor);
            averagenum.Text = "Середнє арефметичне:" + av;
        }

        private void change_Click(object sender, EventArgs e)
        {
            double av = functions.CalculateAverage(vektor);
            functions.NumToZero(vektor, av);
        }
    }
}

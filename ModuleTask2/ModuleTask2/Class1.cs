﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
using System;
using System.IO;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using ExcelApplication = Microsoft.Office.Interop.Excel.Application;
// Створює


namespace ModuleTask2
{
    internal class functions
    {
        public static void FillLastname(DataGridView data)
        {
            Random random = new Random();

            // Заповнюємо рядки від 2 до 7 рандомними прізвищами
            for (int i = 2; i <= 6; i++)
            {
                // Створюємо новий рядок
                DataGridViewRow row = new DataGridViewRow();

                // Генеруємо рандомне прізвище
                string lastname = GenerateRandomLastname(random);

                // Створюємо нову комірку з прізвищем
                DataGridViewCell cell = new DataGridViewTextBoxCell();
                cell.Value = lastname;

                // Добавляємо комірку до рядка
                row.Cells.Add(cell);

                // Добавляємо рядок до DataGridView
                data.Rows.Add(row);
            }
        }

        public static string GenerateRandomLastname(Random random)
        {
            string[] lastnames = { "Шимон", "Новікова", "Сочка", "Кевпанич", "Галь", "Король", "Глуханич","Гриценко"};

            int index = random.Next(lastnames.Length);
            return lastnames[index];
        }

        public static void FillClass(DataGridView data)
        {
            Random random = new Random();

            // Заповнюємо значеннями "A" або "B" третій стовпець рядків
            foreach (DataGridViewRow row in data.Rows)
            {
                // Створюємо нову комірку зі значенням "A" або "B"
                DataGridViewCell cell = new DataGridViewTextBoxCell();
                cell.Value = random.Next(2) == 0 ? "A" : "B";

                // Додаємо комірку до третього стовпця рядка
                row.Cells[2] = cell;
            }
        }

        public static void FillGender(DataGridView data)
        {
            Random random = new Random();

            // Заповнюємо значеннями "A" або "B" третій стовпець рядків
            foreach (DataGridViewRow row in data.Rows)
            {
                DataGridViewCell cell = new DataGridViewTextBoxCell();
                cell.Value = random.Next(2) == 0 ? "Хлопець" : "Дівчина";

                row.Cells[1] = cell;
            }
        }
        public static void FillGrade(DataGridView data, int num)
        {
            Random random = new Random();

            foreach (DataGridViewRow row in data.Rows)
            {

                DataGridViewCell cell = new DataGridViewTextBoxCell();
                cell.Value = random.Next(12);


                row.Cells[num] = cell;
            }
        }

        public static int CountFemale(DataGridView data, string value)
        {
            int femaleCount = 0;

            // Перевіряємо значення другого стовпця в кожному рядку
            foreach (DataGridViewRow row in data.Rows)
            {
                

                // Перевіряємо, чи значення в третьому стовпці рівне переданому значенню
                if (row.Cells[2].Value != null && row.Cells[2].Value.ToString().Equals(value))
                {
                    // Перевіряємо, чи значення в другому стовпці є "Дівчина"
                    if (row.Cells[1].Value != null && row.Cells[1].Value.ToString().Equals("Дівчина"))
                    {
                        femaleCount++;
                    }
                }
            }

            return femaleCount;
        }

        public static double CalculateAverage(DataGridView data, string value)
        {
            double sum = 0;
            int count = 0;

            // Обходимо кожен рядок DataGridView
            foreach (DataGridViewRow row in data.Rows)
            {
                // Перевіряємо, чи значення в третьому стовпці рівне переданому значенню
                if (row.Cells[2].Value != null && row.Cells[2].Value.ToString().Equals(value))
                {
                    // Перевіряємо, чи рядок не є рядком заголовка або новим рядком
                    if (!row.IsNewRow)
                    {
                        // Перевіряємо, чи значення в четвертому стовпці є числовим
                        if (row.Cells[3].Value != null && double.TryParse(row.Cells[3].Value.ToString(), out double valueColumn4))
                        {
                            sum += valueColumn4;
                            count++;
                        }

                        // Перевіряємо, чи значення в п'ятому стовпці є числовим
                        if (row.Cells[4].Value != null && double.TryParse(row.Cells[4].Value.ToString(), out double valueColumn5))
                        {
                            sum += valueColumn5;
                            count++;
                        }
                    }
                }
            }

            // Обчислюємо середнє арифметичне
            if (count > 0)
            {
                double average = sum / count;
                return average;
            }
            else
            {
                return 0;
            }
        }

        public static void ExportToExcel(DataGridView dataGridView, string filePath)
        {
            // Створюємо новий об'єкт Excel
            var excelApp = new ExcelApplication();
            excelApp.Visible = false;

            // Створюємо нову робочу книгу Excel
            var workbook = excelApp.Workbooks.Add(Missing.Value);
            var worksheet = (Worksheet)workbook.ActiveSheet;

            // Заповнюємо заголовки стовпців
            for (int i = 0; i < dataGridView.Columns.Count; i++)
            {
                worksheet.Cells[1, i + 1] = dataGridView.Columns[i].HeaderText;
            }

            // Заповнюємо дані з DataGridView
            for (int i = 0; i < dataGridView.Rows.Count; i++)
            {
                for (int j = 0; j < dataGridView.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = dataGridView.Rows[i].Cells[j].Value?.ToString();
                }
            }

            // Зберігаємо робочу книгу Excel у файл
            workbook.SaveAs(filePath);

            // Закриваємо робочу книгу Excel і звільняємо ресурси
            workbook.Close();
            excelApp.Quit();

            ReleaseObject(worksheet);
            ReleaseObject(workbook);
            ReleaseObject(excelApp);
        }

        // Метод для звільнення ресурсів Excel
        private static void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                Console.WriteLine("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

    }
}

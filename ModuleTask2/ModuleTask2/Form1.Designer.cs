﻿namespace ModuleTask2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Main = new System.Windows.Forms.DataGridView();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Class = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grade1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grade2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountFemaleLabel = new System.Windows.Forms.Label();
            this.inputClass = new System.Windows.Forms.TextBox();
            this.Find = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.AverageGrade = new System.Windows.Forms.Button();
            this.AverageGradeLabel = new System.Windows.Forms.Label();
            this.Generate = new System.Windows.Forms.Button();
            this.Download = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Main)).BeginInit();
            this.SuspendLayout();
            // 
            // Main
            // 
            this.Main.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Main.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LastName,
            this.Gender,
            this.Class,
            this.Grade1,
            this.Grade2});
            this.Main.Location = new System.Drawing.Point(12, 12);
            this.Main.Name = "Main";
            this.Main.Size = new System.Drawing.Size(543, 153);
            this.Main.TabIndex = 0;
            // 
            // LastName
            // 
            this.LastName.HeaderText = "Прізвище";
            this.LastName.Name = "LastName";
            // 
            // Gender
            // 
            this.Gender.HeaderText = "Стать";
            this.Gender.Name = "Gender";
            // 
            // Class
            // 
            this.Class.HeaderText = "Клас";
            this.Class.Name = "Class";
            // 
            // Grade1
            // 
            this.Grade1.HeaderText = "Оцінка1";
            this.Grade1.Name = "Grade1";
            // 
            // Grade2
            // 
            this.Grade2.HeaderText = "Оцінка2";
            this.Grade2.Name = "Grade2";
            // 
            // CountFemaleLabel
            // 
            this.CountFemaleLabel.AutoSize = true;
            this.CountFemaleLabel.Location = new System.Drawing.Point(275, 180);
            this.CountFemaleLabel.Name = "CountFemaleLabel";
            this.CountFemaleLabel.Size = new System.Drawing.Size(89, 13);
            this.CountFemaleLabel.TabIndex = 1;
            this.CountFemaleLabel.Text = "Кількість дівчат:";
            // 
            // inputClass
            // 
            this.inputClass.Location = new System.Drawing.Point(60, 201);
            this.inputClass.Name = "inputClass";
            this.inputClass.Size = new System.Drawing.Size(100, 20);
            this.inputClass.TabIndex = 2;
            // 
            // Find
            // 
            this.Find.Location = new System.Drawing.Point(180, 175);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(75, 23);
            this.Find.TabIndex = 3;
            this.Find.Text = "Кількість дівчат";
            this.Find.UseVisualStyleBackColor = true;
            this.Find.Click += new System.EventHandler(this.Find_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Клас";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // AverageGrade
            // 
            this.AverageGrade.Location = new System.Drawing.Point(180, 223);
            this.AverageGrade.Name = "AverageGrade";
            this.AverageGrade.Size = new System.Drawing.Size(75, 23);
            this.AverageGrade.TabIndex = 5;
            this.AverageGrade.Text = "Знайти";
            this.AverageGrade.UseVisualStyleBackColor = true;
            this.AverageGrade.Click += new System.EventHandler(this.AverageGrade_Click);
            // 
            // AverageGradeLabel
            // 
            this.AverageGradeLabel.AutoSize = true;
            this.AverageGradeLabel.Location = new System.Drawing.Point(276, 228);
            this.AverageGradeLabel.Name = "AverageGradeLabel";
            this.AverageGradeLabel.Size = new System.Drawing.Size(88, 13);
            this.AverageGradeLabel.TabIndex = 6;
            this.AverageGradeLabel.Text = "Середня оцінка:";
            // 
            // Generate
            // 
            this.Generate.Location = new System.Drawing.Point(611, 34);
            this.Generate.Name = "Generate";
            this.Generate.Size = new System.Drawing.Size(121, 46);
            this.Generate.TabIndex = 7;
            this.Generate.Text = "Згенерувати";
            this.Generate.UseVisualStyleBackColor = true;
            this.Generate.Click += new System.EventHandler(this.Generate_Click);
            // 
            // Download
            // 
            this.Download.Location = new System.Drawing.Point(611, 108);
            this.Download.Name = "Download";
            this.Download.Size = new System.Drawing.Size(121, 47);
            this.Download.TabIndex = 8;
            this.Download.Text = "Загрузити";
            this.Download.UseVisualStyleBackColor = true;
            this.Download.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 251);
            this.Controls.Add(this.Download);
            this.Controls.Add(this.Generate);
            this.Controls.Add(this.AverageGradeLabel);
            this.Controls.Add(this.AverageGrade);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.inputClass);
            this.Controls.Add(this.CountFemaleLabel);
            this.Controls.Add(this.Main);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Main)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Main;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn Class;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade2;
        private System.Windows.Forms.Label CountFemaleLabel;
        private System.Windows.Forms.TextBox inputClass;
        private System.Windows.Forms.Button Find;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AverageGrade;
        private System.Windows.Forms.Label AverageGradeLabel;
        private System.Windows.Forms.Button Generate;
        private System.Windows.Forms.Button Download;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModuleTask2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Find_Click(object sender, EventArgs e)
        {
            string nameOfClass = inputClass.Text;
            int countFemale = functions.CountFemale(Main, nameOfClass);
            CountFemaleLabel.Text = "Кількість дівчат:" + Convert.ToString(countFemale);
        }

        private void AverageGrade_Click(object sender, EventArgs e)
        {

            string nameOfClass = inputClass.Text;
            double countFemale = functions.CalculateAverage(Main, nameOfClass);
            AverageGradeLabel.Text = "Середня оцінка:" + Convert.ToString(countFemale);
        }

        private void Generate_Click(object sender, EventArgs e)
        {
            functions.FillLastname(Main);
            functions.FillClass(Main);
            functions.FillGender(Main);
            functions.FillGrade(Main, 3);
            functions.FillGrade(Main, 4);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filePath = "C:\\Users\\tioma\\Desktop\\example.xlsx";
            functions.ExportToExcel(Main, filePath);

        }
    }
}
